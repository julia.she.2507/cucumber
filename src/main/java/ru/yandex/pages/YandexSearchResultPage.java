package ru.yandex.pages;

import com.google.inject.Inject;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import support.GuiceExample;

public class YandexSearchResultPage extends BasePage<YandexSearchResultPage> {

    @FindBy(css = "ul li div h2")
    private WebElement firstResult;

    @Inject
    public YandexSearchResultPage(GuiceExample guiceExample){
        super(guiceExample, "/search");
    }

    public String getFirstResulText(){
        return firstResult.getText();
    }
}
