package ru.yandex.pages;

import com.google.inject.Inject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import support.GuiceExample;

public class YandexMainPage extends BasePage<YandexMainPage> {

    @FindBy(id = "text")
    private WebElement searchField;
    @FindBy(tagName = "button")
    private WebElement searchButton;

    @Inject
    public YandexMainPage(GuiceExample guiceExample){
        super(guiceExample, "");
    }

    public YandexMainPage enterText(String text){
        searchField.sendKeys(text);
        return this;
    }

    public void clickSearchButton(){
        searchButton.click();
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public String getPlaceholderText(){
        return searchField.getAttribute("placeholder");
    }
}
