package ru.yandex.steps;

import com.google.inject.Inject;
import io.cucumber.java.ru.Тогда;
import org.junit.jupiter.api.Assertions;
import ru.yandex.pages.YandexSearchResultPage;

public class YandexSearchResultPageSteps {

    @Inject
    YandexSearchResultPage yandexSearchResultPage;

    @Тогда("Первая ссылка в выдаче имеет тест {string}")
    public void assertFirsLinkText(String expectedText){
        Assertions.assertEquals(expectedText, yandexSearchResultPage.getFirstResulText());
    }
}
