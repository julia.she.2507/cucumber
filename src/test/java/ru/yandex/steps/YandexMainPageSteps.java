package ru.yandex.steps;

import com.google.inject.Inject;
import io.cucumber.java.ru.Если;
import io.cucumber.java.ru.Пусть;
import io.cucumber.java.ru.Тогда;
import org.junit.jupiter.api.Assertions;
import ru.yandex.pages.YandexMainPage;

public class YandexMainPageSteps {

    @Inject
    private YandexMainPage yandexMainPage;

    @Пусть("Открыта главная страница в браузере")
    public void openMainPage(){
        yandexMainPage.open();
    }

    @Тогда("Проверить placeholder")
    public void checkPlaceholder(){
        Assertions.assertEquals("Найдётся всё", yandexMainPage.getPlaceholderText());
    }

    @Если("Ввести в поисковую строку {string}")
    public void enterText(String text){
        yandexMainPage.enterText(text);
    }

    @Если("Нажать кнопку поиск")
    public void clickFindButton(){
        yandexMainPage.clickSearchButton();
    }

}
