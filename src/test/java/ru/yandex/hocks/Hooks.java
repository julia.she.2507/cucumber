package ru.yandex.hocks;

import com.google.inject.Inject;
import io.cucumber.java.After;
import support.GuiceExample;

public class Hooks {
    @Inject
    private GuiceExample guiceExample;
    @After()
    public void closeDriver(){
        if (guiceExample.driver != null)
            guiceExample.driver.quit();
    }
}
